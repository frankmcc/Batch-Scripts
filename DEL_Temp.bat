:: Delete Temp Files - Frank McCourry
ECHO OFF

del %TEMP%\*.* /Q
del %TMP%\*.* /Q

:: Clean up Empty Folders
PUSHD %TEMP%
for /f "delims=" %%d in ('dir /s /b /ad %1 ^| sort /r') do rd "%%d" 2>null

PUSHD %TMP%
for /f "delims=" %%d in ('dir /s /b /ad %1 ^| sort /r') do rd "%%d" 2>null
