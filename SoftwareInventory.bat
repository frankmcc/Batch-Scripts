ECHO
SETLOCAL
Set logpath=C:\Logs\SoftwareInventory
SET computername1=%COMPUTERNAME%.%USERDNSDOMAIN%
SET mappath="%HOMEDRIVE%%HOMEPATH%\Documents\z-%computername1% - Reload Info\Software Inventory"
SET RetentionDays=30
::SET MapDrive=V:
IF NOT EXIST %mappath% mkdir %mappath%
IF NOT EXIST %logpath% mkdir %logpath%

CALL :Main >%logpath%\%computername1%.log.txt 2>&1
GOTO EXIT

:Main
set hh=%time:~0,2%
if "%time:~0,1%"==" " set hh=0%hh:~1,1%
set time0=%hh%%TIME:~3,2%%TIME:~6,2%
set filename=\%computername1%.%date:~-4,4%-%date:~-10,2%-%date:~7,2%-%time0:~0,2%.%time0:~2,2%.%time0:~4,2%.txt

::echo %computername1%
::echo %filename%
::echo %MapDrive%%filename%
::echo %logpath%\%computername1%

::net use /D %MapDrive%
::net use %MapDrive% %mappath%

::call C:\SysinternalsSuite\psexec \\%computername1% -h C:\SysinternalsSuite\psinfo.exe -s >%MapPath%%filename%
call C:\SysinternalsSuite\psinfo.exe -s >%MapPath%%filename%

FORFILES /P %MapPath% /M %computername1%.*.txt /D -%retentiondays% /C "cmd /c del @file"

echo %ERRORLEVEL%
IF NOT ERRORLEVEL 0 GOTO Continue
GOTO Continue

:Continue
::net use /D %MapDrive%

:Exit