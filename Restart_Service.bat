::RESTART_Service
::Written by Frank J. McCourry
::2/2020

ECHO OFF
:: Set all variables Local
SETLOCAL

::Set USELOG to "Yes" to create a log and "No" to suppress all output. 
SET USELOG=No

::Set LogFile Location. Create the folder and file if necessary.
:SetLogFilePath
IF %USELOG%==No SET LogFile=null
If %USELOG%==No Goto SetServers
SET LogFileDrive=C:
SET LogFilePath=\LOGS\
SET LogFileName=ServiceName_Restart.log.txt
If NOT EXIST %LogFileDrive%%LogFilePath% (
CD %LogFileDrive%
MKDIR %LogFilePath% 
)

:SetLogfile
SET LogFile=%LogFileDrive%%LogFilePath%%LogFileName%

:SetServers
::Set Variables for Servers by Name or IP address.  You can use any number of variables here, just keep them sequential.
SET Svr[1]=SERVER_HOSTNAME 
::SET Svr[2]=
::SET Svr[3]=
::SET Svr[4]=

:SetServiceName
:: Use the name of the service as defined by the Service Name field in the Service Properties
Set ServiceName=QuickBooksDB30

::=======================THERE IS NO NEED TO CHANGE ANYTHING BELOW========================

::Clear the Screen, it just looks nice.
CLS
ECHO.
::Create Log File if needed
ECHO Creating Log %LogFile%
ECHO %Date% %Time% Logfile Created 2>>%LogFile%
ECHO.

::Start Processing
FOR /F "tokens=2 delims==" %%A IN ('SET Svr[') DO (
ECHO Processing Commands for %%A 2>>%LogFile%
ECHO.


:StopService
sc \\%%A stop %ServiceName% >>%LogFile%
ECHO Stopping %ServiceName% Service on %%A
If ERRORLEVEL 1 Goto StopService
Call :TimeOut
ECHO.
ECHO %%A %ServiceName% Status 2>>%LogFile%
ECHO ======================================
sc \\%%A query %ServiceName% | findstr /i "STATE" 2>>%LogFile%
ECHO.

:StartService
ECHO Starting %ServiceName% on %%A
sc \\%%A start %ServiceName% >>%LogFile%
If ERRORLEVEL 1 Goto StartService
Call :TimeOut
ECHO.
ECHO %%A %ServiceName% Status 2>>%LogFile%
ECHO ======================================
sc \\%%A query %ServiceName% | findstr /i "STATE" 2>>%LogFile%
ECHO.
)

::Cleanup
ENDLOCAL

:END
exit /b

:TimeOut
Timeout /t 10 /nobreak
CLS
GOTO :EOF
