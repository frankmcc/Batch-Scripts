# Batch Scripts for DOS or Windows
DISCLAIMER - USE THESE AT YOUR OWN RISK - Batch programs can do a lot of damage if used improperly.  You can either use these as is or modify them to suit your needs.  Read them, know what you are doing! Just don't blame me if something goes wrong.  

- DEL_Temp.bat - Cleanup your temp folders in Windows or DOS
- Restart_Service.bat  - Restart any Windows or DOS service.  Usefull when creating a task.
- Software Inventory - Creates a text file with a list of all software on a system.  Requires Sysinternals PSInfo.exe

